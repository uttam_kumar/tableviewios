//
//  ViewController.swift
//  TableView
//
//  Created by Syncrhonous on 30/1/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

var dataList = ["New York, NY", "Los Angeles, CA", "Chicago, IL", "Houston, TX",
              "Philadelphia, PA", "Phoenix, AZ", "San Diego, CA", "San Antonio, TX",
              "Dallas, TX", "Detroit, MI", "San Jose, CA", "Indianapolis, IN",
              "Jacksonville, FL", "San Francisco, CA", "Columbus, OH", "Austin, TX",
              "Memphis, TN", "Baltimore, MD", "Charlotte, ND", "Fort Worth, TX"]


var indicator=0
var filteredData: [String]!



class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchBar.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TableCell")
        
        cell.textLabel?.text=dataList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indicator=indexPath.row
        performSegue(withIdentifier: "connector", sender: self)
    }
    
    

}

